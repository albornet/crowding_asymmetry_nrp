#!/bin/bash

function log() {
    echo "[Crowding experiment] $*"
}

# function download_saliency () {
#     if [ $1 ]; then
#         curl -k -o model.ckpt.pb "https://neurorobotics-files.net/remote.php/webdav/WP%20working%20directories/CDP4/Models/Saliency/GPU/model.pb"
#         curl -k -o model.ckpt.meta "https://neurorobotics-files.net/remote.php/webdav/WP%20working%20directories/CDP4/Models/Saliency/GPU/model.ckpt.meta"
#         curl -k -o model.ckpt.index "https://neurorobotics-files.net/remote.php/webdav/WP%20working%20directories/CDP4/Models/Saliency/GPU/model.ckpt.index"
#         curl -k -o model.ckpt.data-00000-of-00001 "https://neurorobotics-files.net/remote.php/webdav/WP%20working%20directories/CDP4/Models/Saliency/GPU/model.ckpt.data-00000-of-00001"
#         echo "gpu" > config
#     else
#         curl -k -o model.ckpt.pb "https://neurorobotics-files.net/remote.php/webdav/WP%20working%20directories/CDP4/Models/Saliency/CPU/model.pb"
#         curl -k -o model.ckpt.meta "https://neurorobotics-files.net/remote.php/webdav/WP%20working%20directories/CDP4/Models/Saliency/CPU/model.ckpt.meta"
#         curl -k -o model.ckpt.index "https://neurorobotics-files.net/remote.php/webdav/WP%20working%20directories/CDP4/Models/Saliency/CPU/model.ckpt.index"
#         curl -k -o model.ckpt.data-00000-of-00001 "https://neurorobotics-files.net/remote.php/webdav/WP%20working%20directories/CDP4/Models/Saliency/CPU/model.ckpt.data-00000-of-00001"
#         echo "cpu" > config
#     fi
# }

printf "\033[1;33mWould you like to install the GPU version of tensorflow? (y/n)\033[0m\n"
read tf_gpu
if [ "$tf_gpu" == "Y" -o "$tf_gpu" == "y" ]
then
  echo "The gpu versions of models will be downloaded"
  gpu=1
else
  echo "The cpu versions of models will be downloaded"
  unset gpu
fi


MODELS=$HBP/Models
EXPERIMENTS=$HBP/Experiments
echo
echo -----------------------------------------
echo Installing Conjoint WP2 Experiment in NRP
echo -----------------------------------------
echo
echo "Checking your NRP installation"
echo
echo "\$HBP: ${HBP}"
echo "\$HBP/Models: ${MODELS}"
echo "\$HBP/Experiments: ${EXPERIMENTS}"
echo "\$NRP_VIRTUAL_ENV: ${NRP_VIRTUAL_ENV}"
echo

for dir in ${HBP} ${MODELS} ${EXPERIMENTS} ${NRP_VIRTUAL_ENV}; do
  if [[ ! -d ${dir} ]]; then
    log "The folder ${dir} doesn't exist."
    nrp_folder_not_found=1
  fi
done

nrp_info_link="https://bitbucket.org/hbpneurorobotics/neurorobotics-platform/src/master/"
if [[ ${nrp_folder_not_found} == "1" ]]; then
  log "Please check your NRP installation."
  log "All info available at ${nrp_info_link}."
  exit 1
fi

log "OK: Models, Experiments and platform_venv have been found."
echo
log "Copying models and experiment files"
echo --------------------------------------------

echo
log "Copying models into ${MODELS}"
cp ExpModels/visual_segmentation_3.py ${MODELS}/brain_model
cp -R ExpModels/crowding_virtuallab ${MODELS}
cp -R ExpModels/icub_static_model ${MODELS}
log "Custom retina model overwrites the one in the NRP directory"
rm -rf ${HBP}/retina
unzip -d ${HBP} ExpModels/customR.zip
if [ "$tf_gpu" == "Y" -o "$tf_gpu" == "y" ]
then
  unzip -d ${HBP}/Models/brain_model ExpModels/customG.zip
else
  unzip -d ${HBP}/Models/brain_model ExpModels/customC.zip
fi

echo
log "Executing \$HBP/Models/create-symlinks.sh"
bash ${MODELS}/create-symlinks.sh # Create symlinks in ~/.gazebo/models and $HBP/gzweb/http/client/assets

# Copy the content of this experiment folder into $HBP/Experiments/wp2_featuring
excluded_files=(
  ExpModels
  README.md
  install.sh
  uninstall.sh
  .git
)
for f in ${excluded_files[@]}; do
  excluded_paths="${excluded_paths} --exclude=${f}"
done
target_folder=${EXPERIMENTS}/wp2_featuring
source_folder=`dirname $0`
echo
log "Copying files to ${target_folder}"
rsync -av ${excluded_paths} ${source_folder} ${target_folder}

echo
echo
log "Installing dependencies"
echo ----------------------------------------

# echo
# log "Downloading the saliency model"
# echo  
# cd ${MODELS}/brain_model
# mkdir -p saliency
# cd saliency
# current_setup=`cat config`
# if [ -z $current_setup ]; then
#     download_saliency $gpu
# elif [ $current_setup  == "gpu" ] && [ $gpu ]; then
#     log "GPU weights already present"
# elif [ $current_setup == "cpu" ] && [ -z $gpu ]; then
#     log "CPU weights already present"
# else
#     download_saliency $gpu
# fi

log "Installing tensorflow in ${HOME}/.opt/tensorflow_venv"
echo
cd ${HOME}/.opt  
virtualenv --system-site-packages tensorflow_venv
source tensorflow_venv/bin/activate
if [ $gpu ]; then
  pip install tensorflow-gpu==1.12.0 || { log TENSORFLOW-GPU: INSTALL ERROR; exit 1; }
else
  pip install tensorflow==1.12.0 || { log TENSORFLOW-CPU: INSTALL ERROR; exit 1; }
fi
deactivate

log "Installing custom version of the retina"
cd ${HBP}/retina
mkdir build
cd build
qmake-qt4 ../retina.pro INSTALL_PREFIX=$HBP/retina/build CONFIG+=release CONFIG+=nodisplay
make -j8 && make install

cd $DIR
echo
log "Installation of WP2 Conjoint experiment: DONE"
log "Congrats!"
