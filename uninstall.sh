#!/bin/bash
MODELS=$HBP/Models
EXPERIMENTS=$HBP/Experiments
echo
echo ---------------------------------------------
echo Uninstalling Conjoint WP2 Experiment from NRP
echo ---------------------------------------------
echo

rm -f ${MODELS}/brain_model/visual_segmentation_3.py
rm -rf ${MODELS}/crowding_virtuallab
rm -rf ${MODELS}/icub_static_model
rm -rf ${MODELS}/brain_model/saliency
bash ${MODELS}/create-symlinks.sh > /dev/null
rm -rf ${EXPERIMENTS}/wp2_featuring
echo
echo "Uninstalling: DONE"