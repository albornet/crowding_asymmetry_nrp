# Imported python transfer function that computes the template match from the segmentation output
import numpy
from sensor_msgs.msg import Image
from std_msgs.msg import String, Float32
from cv_bridge import CvBridge
@nrp.MapRobotSubscriber('V2Activity',     Topic('/robot/V2Activity', Image  ))
@nrp.MapRobotSubscriber('stimType',       Topic('/robot/stim_type',  String ))
@nrp.MapRobotSubscriber('stimSide',       Topic('/robot/stim_side',  String ))
@nrp.MapRobotSubscriber('intRange',       Topic('/robot/int_range',  Float32))
@nrp.MapRobotSubscriber('intStren',       Topic('/robot/int_stren',  Float32))
@nrp.MapRobotSubscriber('numTrial',       Topic('/robot/num_trial',  Float32))
@nrp.MapRobotPublisher( 'target_plot',    Topic('/robot/target',     Image  ))
@nrp.MapRobotPublisher( 'signal_plot',    Topic('/robot/signal',     Image  ))
@nrp.MapRobotPublisher( 'noise_plot',     Topic('/robot/noise',      Image  ))
@nrp.MapRobotPublisher( 'result_plot',    Topic('/robot/resultPlot', Image  ))
@nrp.MapVariable(       'targetMean',     initial_value=numpy.zeros((nrp.config.brain_root.oriRows, nrp.config.brain_root.oriCols)))
@nrp.MapVariable(       'targetIndex',    initial_value=0)
@nrp.MapVariable(       'targetMatches',  initial_value=[])
@nrp.MapVariable(       'temporaryMatch', initial_value=[0.0]*nrp.config.brain_root.nSegLayers)
@nrp.MapVariable(       'isTemplating',   initial_value=None, scope=nrp.GLOBAL)
@nrp.MapVariable(       'isComputing',    initial_value=None, scope=nrp.GLOBAL)
@nrp.MapVariable(       'justComputed',   initial_value=None, scope=nrp.GLOBAL)
@nrp.MapVariable(       'isPlotting',     initial_value=None, scope=nrp.GLOBAL)
def LOG_compute_match(t, V2Activity, stimType, stimSide, intRange, intStren, numTrial, target_plot, signal_plot, noise_plot, result_plot, targetMean, targetIndex, targetMatches, temporaryMatch, isTemplating, isComputing, justComputed, isPlotting):

    # Parameters and imports
    import numpy
    nSegs = nrp.config.brain_root.nSegLayers
    nRows = nrp.config.brain_root.oriRows
    nCols = nrp.config.brain_root.oriCols

    # Initialize plot variables
    plot_signal = numpy.zeros((nSegs*(nRows+1)-1,nCols), dtype=numpy.uint8)
    plot_noise  = numpy.zeros((nSegs*(nRows+1)-1,nCols), dtype=numpy.uint8)
    for h in range(1,nSegs):
        plot_signal[h*(nRows+1)-1,:] = 254  # white line
        plot_noise[ h*(nRows+1)-1,:] = 254  # white line

    # Control that V2Activity exists
    if V2Activity.value is not None:

        # Load V2 activity (all segmentation layers)
        V2_raw = CvBridge().imgmsg_to_cv2(V2Activity.value).mean(axis=2)

        # Build the target template before doing template match
        if isTemplating.value:
            
            # Compute the mean of the 1st seg. layer's activity over time-steps (no seg. before template is built)
            V2_first          = 5.0*V2_raw[0*(nRows+1):(0+1)*(nRows+1)-1,:]
            targetMean.value  = 1.0/(targetIndex.value+1)*(targetIndex.value*targetMean.value + V2_first)
            targetIndex.value = targetIndex.value+1

        # Compute the template match using the target template
        elif isComputing.value and not isTemplating.value:
            
            # Imports and initialization
            from scipy.signal import convolve2d
            from copy import deepcopy
            thisTrialMatches = []
            signal_raw       = []
            sigma            = intRange.value.data
            I0               = intStren.value.data

            # First, fiddle with the target to find its exact location
            max_match = 0
            targ_test = targetMean.value
            for h in range(nSegs):

                # Collect the signal image of the current segmentation layer
                signal_raw.append(V2_raw[h*(nRows+1):(h+1)*(nRows+1)-1,:])

                # Fiddle part (the robot's eyes still move for a very long time)
                for i in range(-1,2):
                    for j in range(-1,2):
                        test       = numpy.roll(numpy.roll(targ_test, i, axis=0), j, axis=1)
                        match_test = (test*signal_raw[h]).sum()
                        
                        # Slightly shifts the target template if better
                        if match_test > max_match:
                            max_match        = match_test
                            targetMean.value = deepcopy(test)

            # Then, build noise and signal to compute the match
            target = targetMean.value/targetMean.value.max()
            for h in range(nSegs):

                # Initialize the signal and the noise arrays
                signal = deepcopy(signal_raw[h])
                noise  = deepcopy(signal_raw[h])

                # Build the arrays, using the target template (shift for less noise)
                t0 = target
                t1 = numpy.roll(target, 1, axis=0)
                t2 = numpy.roll(target,-1, axis=0)
                t3 = numpy.roll(target, 1, axis=1)
                t4 = numpy.roll(target,-1, axis=1)
                signal[(t0 <  0.2) * (t1 <  0.2) * (t2 <  0.2) * (t3 <  0.2) * (t4 <  0.2)] = 0.0
                noise[ (t0 >= 0.2) + (t1 >= 0.2) + (t2 >= 0.2) + (t3 >= 0.2) + (t4 >= 0.2)] = 0.0
            
                # Feed the plot arrays with the signal and noise
                plot_signal[h*(nRows+1):(h+1)*(nRows+1)-1] = signal
                plot_noise[ h*(nRows+1):(h+1)*(nRows+1)-1] = noise

                # The noise interferes with the signal, distance-dependent
                r      = numpy.arange(0, nRows, 1)
                c      = numpy.arange(0, nCols, 1)
                cc, rr = numpy.meshgrid(c, r)
                weight = numpy.exp(-numpy.sqrt((rr-1-nRows/2)**2 + (cc-1-nCols/2)**2)/sigma)*I0
                remain = signal - convolve2d(noise, weight, mode='valid')
             
                # Add this step match to the final value and send it if last step of computation   
                temporaryMatch.value[h] = temporaryMatch.value[h] + remain[remain>0].sum()/3000.0
                if justComputed.value:

                    # Send the match value and reset the match computation
                    thisTrialMatches.append(temporaryMatch.value[h])
                    temporaryMatch.value[h] = 0.0

                    # Reset the signal and noise plots, because they are not relevant
                    plot_signal[h*(nRows+1):(h+1)*(nRows+1)-1] = numpy.zeros((nRows, nCols))
                    plot_noise[ h*(nRows+1):(h+1)*(nRows+1)-1] = numpy.zeros((nRows, nCols))
           
            # Record this trial's template match computation
            if justComputed.value:
                targetMatches.value.append(thisTrialMatches)
                clientLogger.info(str(targetMatches.value))
      
    # Build the plot arrays
    plot_signal = numpy.dstack((plot_signal,      plot_signal,      plot_signal     ))
    plot_noise  = numpy.dstack((plot_noise,       plot_noise,       plot_noise      ))
    plot_mean   = numpy.dstack((targetMean.value, targetMean.value, targetMean.value))
 
    # Build the messages to plot
    msg_signal = CvBridge().cv2_to_imgmsg(plot_signal.astype(numpy.uint8), 'rgb8')
    msg_mean   = CvBridge().cv2_to_imgmsg(plot_mean.  astype(numpy.uint8), 'rgb8')
    msg_noise  = CvBridge().cv2_to_imgmsg(plot_noise. astype(numpy.uint8), 'rgb8')

    # Send the messages to the rostopic
    signal_plot.send_message(msg_signal)
    noise_plot. send_message(msg_noise )
    target_plot.send_message(msg_mean  )
    
    # Plot the final figure, after all the trials are recorded
    if isPlotting.value:

        # Initialization
        import matplotlib.pyplot as plt
        nTrials    = numTrial.value.data
        matches    = targetMatches.value
        match_list = [[], []]

        # Collect the maximum match for each trial
        for n in range(len(matches)):
            if n%(nTrials*2) > (nTrials-1):
                match_list[1].append(max(matches[n]))
            else:
                match_list[0].append(max(matches[n]))

        # Assign each trial to 'In' and 'Out' conditions
        if stimSide.value.data == 'Out':
            matches_in  = match_list[1]
            matches_out = match_list[0]
        else:
            matches_in  = match_list[0]
            matches_out = match_list[1]

        # Select the values associated to the condition
        if stimType.value.data == 'Vernier':
            target  = 40.0
            perfect = 60.0
        if stimType.value.data == 'Mooney_3':
            target  = 40.0
            perfect = 60.0
        if stimType.value.data == 'Mooney_6':
            target  = 40.0
            perfect = 60.0
        if stimType.value.data == 'Mooney_10':
            target  = 40.0
            perfect = 60.0

        # Compute matches, threhsolds and elevations
        m_t, m_o, m_i =  target, numpy.array(matches_out).mean(), numpy.array(matches_in).mean()
        t_t, t_o, t_i = [perfect-a for a in [m_t, m_o, m_i]]
        e_t, e_o, e_i = [a/t_t for a in [t_t, t_o, t_i]]
        
        # Plot the elevation for all conditions
        label = ['Out', 'In']
        index = numpy.arange(len(label))
        fig   = plt.figure()
        plt.bar(index, [e_o, e_i], width=0.5, align='center', alpha=0.5)
        plt.ylabel('Threshold elevation', fontsize=18)
        plt.xticks(index, label, fontsize=18)
        plt.title('Live plot (updated at each trial)', fontsize=24)
        plt.plot([-0.5,1.5], [e_t, e_t], 'k--')

        # Send the figure to a cv2 image
        fig.canvas.draw()
        img = numpy.fromstring(fig.canvas.tostring_rgb(), dtype=numpy.uint8, sep='')
        img = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))

        # Send the message to display and close the figure
        msg_frame = CvBridge().cv2_to_imgmsg(img, 'rgb8')
        result_plot.send_message(msg_frame)
        plt.close()
