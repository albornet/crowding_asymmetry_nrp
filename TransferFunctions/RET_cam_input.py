# Imported python transfer function that transforms the camera output into a DC current source fed to LGN neurons
import sensor_msgs.msg
@nrp.MapRobotSubscriber('camera',       Topic('/robot/icub_model/right_eye_camera/image_raw', sensor_msgs.msg.Image))
@nrp.MapRobotPublisher( 'Input',        Topic('/robot/Input',                                 sensor_msgs.msg.Image))
@nrp.MapRobotSubscriber('initTime',     Topic('/robot/init_time',                             Float32))
@nrp.MapSpikeSource(    'LGN',          nrp.map_neurons(range(0, nrp.config.brain_root.nPols*nrp.config.brain_root.nRows*nrp.config.brain_root.nCols), lambda i: nrp.brain.LGN[i]), nrp.dc_source)
@nrp.MapVariable(       'eccentricity', initial_value=None,          scope=nrp.GLOBAL)  # (row, col), right/bottom is positive
@nrp.MapVariable(       'camSpanAngle', initial_value=[34.31,45.75], scope=nrp.GLOBAL)  # in degrees, also (row, col)
@nrp.Robot2Neuron()
def RET_cam_input(t, camera, Input, initTime, LGN, eccentricity, camSpanAngle):

    # Values in displayStim.exd take time to initialize, so this avoids a bug
    if t > 0.2:

        # Take the image from the robot's left eye
        if camera.value is not None and t > initTime.value.data:

            # Parameters and imports
            import numpy
            nRows     = nrp.config.brain_root.nRows
            nCols     = nrp.config.brain_root.nCols
            LGNBGain  = 1.0
            LGNDGain  = 1.0
            greyValue = 127.0  # between 0.0 and 254.0 ; neutral is 127.0

            # Read the image into an array, mean over 3 colors, resize it for the network and flatten the result
            img = numpy.mean(CvBridge().imgmsg_to_cv2(camera.value, 'rgb8'), axis=2)
            
            # Transform the camera output to feed the segmentation model
            eccTan   = [numpy.tan(e*numpy.pi/180.0) for e in eccentricity.value]
            camTan   = [numpy.tan(c*numpy.pi/180.0) for c in camSpanAngle.value]
            shift    = [e/c*s/2.0 for e,c,s in zip(eccTan, camTan, img.shape)]
            firstRow = int((img.shape[0]-nRows)/2 + shift[0])
            firstCol = int((img.shape[1]-nCols)/2 + shift[1])
            imgIn    = 1.45*img[firstRow:firstRow+nRows, firstCol:firstCol+nCols]

            # Feed the LGN cells with the cropped image input (both polarities)
            LGNBInput     = LGNBGain*numpy.maximum(0.0, (imgIn.flatten()-greyValue) / (254.0-greyValue))
            LGNDInput     = LGNDGain*numpy.maximum(0.0, (greyValue-imgIn.flatten()) / (greyValue - 0.0))
            LGN.amplitude = numpy.hstack((LGNBInput, LGNDInput))

            # Debug messages
            InputRGB = numpy.dstack((imgIn, imgIn, imgIn))
            Input.send_message(CvBridge().cv2_to_imgmsg(InputRGB.astype(numpy.uint8), 'rgb8'))