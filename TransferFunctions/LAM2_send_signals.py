# Imported python transfer function that sends segmentation signals, following top-down and bottom-up cues (dumb choice of position for now)
import numpy
from sensor_msgs.msg import Image
from std_msgs.msg import String, Bool
@nrp.MapRobotSubscriber('saliency_map',  Topic('/robot/saliency_map',                          Image ))
@nrp.MapRobotSubscriber('image',         Topic('/robot/icub_model/right_eye_camera/image_raw', Image ))
@nrp.MapRobotSubscriber('stimSide',      Topic('/robot/stim_side',                             String))
@nrp.MapRobotSubscriber('doCortMagnif',  Topic('/robot/do_cort_m',                             Bool  ))
@nrp.MapRobotSubscriber('cropSaliency',  Topic('/robot/do_crop_s',                             Bool  ))
@nrp.MapSpikeSource(    'segSignalOn',   nrp.map_neurons(range(nrp.config.brain_root.useSegmentation*(nrp.config.brain_root.nSegLayers-1)*nrp.config.brain_root.oriRows*nrp.config.brain_root.oriCols), lambda i: nrp.brain.V2SegNet[i]), nrp.dc_source, amplitude=0.0)
@nrp.MapVariable(       'plotSegSignal', scope=nrp.GLOBAL, initial_value= numpy.zeros((nrp.config.brain_root.nSegLayers-1, nrp.config.brain_root.nRows, nrp.config.brain_root.nCols)))
@nrp.MapVariable(       'isSegmenting',  scope=nrp.GLOBAL, initial_value= False       )
@nrp.MapVariable(       'eccentricity',  scope=nrp.GLOBAL, initial_value= None        )  # (row, col), right/bottom is positive
@nrp.MapVariable(       'camSpanAngle',  scope=nrp.GLOBAL, initial_value=[34.31,45.75])  # in degrees, also (row, col)
@nrp.MapVariable(       'topLeftCoord',  scope=nrp.GLOBAL, initial_value= None        )
@nrp.Robot2Neuron()
def LAM2_send_signals(t, saliency_map, image, stimSide, segSignalOn, plotSegSignal, isSegmenting, doCortMagnif, cropSaliency, eccentricity, camSpanAngle, topLeftCoord):

    # This TF does stuff only if segmentation is active
    if nrp.config.brain_root.useSegmentation and eccentricity.value is not None:

        nSegLayers = nrp.config.brain_root.nSegLayers

        # Send the signal at the right time-step
        if isSegmenting.value and nSegLayers > 1:

            # Imports
            import numpy
            import random

            # Parameters initialization
            nRows       = nrp.config.brain_root.nRows
            nCols       = nrp.config.brain_root.nCols
            signalSize  = nrp.config.brain_root.segSignalSize
            imgShape    = (960,1280)  # temporary solution (rows, cols)

            # Segmentation initialization
            segSignalmap = numpy.zeros(((nRows+1)*(nCols+1)*(nSegLayers-1),))

            # Localize the top-left origin of the segmentation network
            eccTan   = [numpy.tan(e*numpy.pi/180.0) for e in eccentricity.value]
            camTan   = [numpy.tan(c*numpy.pi/180.0) for c in camSpanAngle.value]
            shift    = [e/c*s/2.0 for e,c,s in zip(eccTan, camTan, imgShape)]
            firstRow = int((imgShape[0]-nRows)/2 + shift[0])
            firstCol = int((imgShape[1]-nCols)/2 + shift[1])

            # Collect output of the saliency computation model
            if saliency_map.value is not None:

                # Pre-process the saliency map message (non-linearity to trigger activity in the segmentation layers))
                salThrsh = 0.4
                salArray = CvBridge().imgmsg_to_cv2(saliency_map.value, 'rgb8')
                salArray = salArray.mean(axis=2)
                salArray = salArray-salThrsh*salArray.max()
                salArray[salArray<0.0] = 0.0
                
                # Crop the saliency map at the Laminart input location (or not)
                salGenShape  = salArray.shape
                if cropSaliency.value.data:
                    salArray = salArray[firstRow:firstRow+nRows, firstCol:firstCol+nCols]

            # Transformed coordinates (could be done only once per simulation, but here it is done at every time-steps)
            if doCortMagnif.value.data and topLeftCoord.value is not None:
                from specs import cart2cort
                xTS, yTS, dS, resS = cart2cort(imgShape[0], imgShape[1], imgShape[0], imgShape[1])

            # Loop for every non basal segmentation layer
            for h in range(nSegLayers-1):

                # Select the target according to the saliency computation (stimulus-driven selection signal)
                if saliency_map.value is not None:

                    # Use the saliency map as a 2D probability density distribution
                    choiceArray            = numpy.array(range(salArray.size))
                    ravelRowCol            = numpy.random.choice(choiceArray, p=numpy.ravel(salArray/float(salArray.sum())))
                    (segLocRow, segLocCol) = numpy.unravel_index(ravelRowCol, salArray.shape)
                    
                    # Shift the signal location (doCortMagnif requires the global coordinates, and local otherwise)
                    if doCortMagnif.value.data and topLeftCoord.value is not None:
                        if cropSaliency.value.data:
                            segLocCol = segLocCol + firstCol
                            segLocRow = segLocRow + firstRow
                    else:
                        if not cropSaliency.value.data:
                            segLocCol = segLocCol - firstCol
                            segLocRow = segLocRow - firstRow

                # Select the target according to the task (task-driven selection signal)
                else:

                    # Use a Gaussian centered on the most successful location as a 2D probability distribution
                    targetCenter  = (nRows/2, nCols/2)
                    (segLocRow, segLocCol) = (int(numpy.random.normal(loc=p, scale=10)) for p in targetCenter)
                    
                    # Shift the signal location (doCortMagnif requires the global coordinates, and local otherwise)
                    if doCortMagnif.value.data and topLeftCoord.value is not None:
                        segLocCol = segLocCol + firstCol
                        segLocRow = segLocRow + firstRow

                # Transform the signal location in cortical coordinates (or not)
                if doCortMagnif.value.data and topLeftCoord.value is not None:
                    segLocRowT = yTS[segLocCol,segLocRow] - topLeftCoord.value[0]  # y is row
                    segLocColT = xTS[segLocCol,segLocRow] - topLeftCoord.value[1]  # x is col
                    (segLocRow, segLocCol) = (segLocRowT, segLocColT)
                        
                # Tells where a signal has been sent
                # clientLogger.info('SL'+str(h+1)+' at (r,c)='+str((segLocRow,segLocCol))+': signal sent after '+str(t)+' s.')

                # Set where the segmentation signal is going to be active
                for i in xrange(max(0, segLocRow-signalSize), min(segLocRow+signalSize, nRows)):
                    for j in xrange(max(0, segLocCol-signalSize), min(segLocCol+signalSize, nCols)):
                        distance = numpy.sqrt(numpy.power(segLocRow-i, 2) + numpy.power(segLocCol-j, 2))
                        if distance < signalSize:
                            segSignalmap[h*(nRows+1)*(nCols+1) + i*(nCols+1) + j] =  10.0
                            plotSegSignal.value[h][i][j]                          = 254.0

            # Send the segmentation signal for the current segmentation layer
            segSignalOn.amplitude = segSignalmap

        # The signal only last sone time step, just enough to start the dynamics
        else:
            segSignalOn.amplitude = 0.0

        # Update the plot (slow decay)
        plotSegSignal.value = (plotSegSignal.value*0.8).astype(np.int16)
