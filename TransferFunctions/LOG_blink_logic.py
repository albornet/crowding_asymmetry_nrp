# Imported python transfer function that transforms the camera input into a DC current source fed to LGN neurons
import numpy, sensor_msgs.msg
from cv_bridge import CvBridge
from std_msgs.msg import String, Float32
@nrp.MapRobotSubscriber('stimType',     Topic('/robot/stim_type', String ))
@nrp.MapRobotSubscriber('initTime',     Topic('/robot/init_time', Float32))
@nrp.MapRobotSubscriber('targTime',     Topic('/robot/targ_time', Float32))
@nrp.MapRobotSubscriber('dispTime',     Topic('/robot/disp_time', Float32))
@nrp.MapRobotSubscriber('numTrial',     Topic('/robot/num_trial', Float32))
@nrp.MapRobotSubscriber('compTime',     Topic('/robot/comp_time', Float32))
@nrp.MapRobotSubscriber('blnkTime',     Topic('/robot/blnk_time', Float32))
@nrp.MapRobotSubscriber('segStart',     Topic('/robot/seg_start', Float32))
@nrp.MapVariable(       'isBlinking',   scope=nrp.GLOBAL, initial_value=None)
@nrp.MapVariable(       'isComputing',  scope=nrp.GLOBAL, initial_value=None)
@nrp.MapVariable(       'justComputed', scope=nrp.GLOBAL, initial_value=None)
@nrp.MapVariable(       'isTemplating', scope=nrp.GLOBAL, initial_value=None)
@nrp.MapVariable(       'isSegmenting', scope=nrp.GLOBAL, initial_value=None)
@nrp.MapVariable(       'isPlotting',   scope=nrp.GLOBAL, initial_value=None)
@nrp.MapVariable(       'eccentricity', scope=nrp.GLOBAL, initial_value=None)
@nrp.MapSpikeSource(    'resetSignal',  nrp.map_neurons(range(1), lambda i: nrp.brain.resetNeuron[i]), nrp.dc_source, amplitude=0.0)
@nrp.Robot2Neuron()
def LOG_blink_logic(t, stimType, initTime, targTime, dispTime, numTrial, compTime, blnkTime, segStart, isBlinking, isComputing, justComputed, isTemplating, isSegmenting, isPlotting, eccentricity, resetSignal):
    
    # Wait that the topics ar initialized (to avoid having missing parameters)
    if t > 0.2:

        # Find out the eccentricity of the stimulus, according to the condition
        eccentricity.value = [0.00, 0.00]  # (row, col), in degrees, right/bottom is positive
        if stimType.value.data == 'Vernier':
            eccentricity.value = [0.00,  3.88]
        if stimType.value.data == 'Mooney_3':
            eccentricity.value = [0.00,  3.00]
        if stimType.value.data == 'Mooney_6':
            eccentricity.value = [0.00,  6.00]
        if stimType.value.data == 'Mooney_10':
            eccentricity.value = [0.00, 10.00]

        # Initialization
        isBlinking.value    = False
        isComputing.value   = False
        justComputed.value  = False
        isTemplating.value  = False
        isSegmenting.value  = False
        isPlotting.value    = False

        # Time-related variables
        loopCount    = int(t/dispTime.value.data)
        lastLoopTime = loopCount*dispTime.value.data
        timeInLoop   = t-lastLoopTime

        # 1) Build the target template before the experiment begins
        if t > initTime.value.data and t < initTime.value.data + targTime.value.data:
            isTemplating.value = True

        # 2) Sends a segmentation
        if t > initTime.value.data + targTime.value.data and segStart.value.data <= timeInLoop < segStart.value.data + 0.02 :
            isSegmenting.value = True

        # 3) Compute template match
        if t > initTime.value.data + targTime.value.data and timeInLoop > dispTime.value.data - compTime.value.data:
            isComputing.value = True

        # 4) Record template match
        if t > initTime.value.data + targTime.value.data and timeInLoop > dispTime.value.data - 0.02:
            justComputed.value = True

        # 5) Reset input and segmentation at each trial
        if t > initTime.value.data and 0.0 <= timeInLoop < blnkTime.value.data:
            isBlinking.value = True
            resetSignal.amplitude = 1000.0
        else:
            resetSignal.amplitude = 0.0

        # 6) Start plotting the final results (threshold elevations)
        if t > initTime.value.data + 2*numTrial.value.data*dispTime.value.data:
            isPlotting.value = True
