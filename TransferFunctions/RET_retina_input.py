# Imported python transfer function that transforms the retina output into a DC current source fed to LGN neurons
from sensor_msgs.msg import Image
from std_msgs.msg import Float32, Bool
from cv_bridge import CvBridge
@nrp.MapRobotSubscriber('image',         Topic('/robot/icub_model/right_eye_camera/image_raw', Image  ))
@nrp.MapRobotPublisher( 'inputONToLGN',  Topic('/robot/LGNBrightInput',                        Image  ))
@nrp.MapRobotPublisher( 'inputOFFToLGN', Topic('/robot/LGNDarkInput',                          Image  ))
@nrp.MapRobotSubscriber('doCortMagnif',  Topic('/robot/do_cort_m',                             Bool   ))
@nrp.MapRobotSubscriber('initTime',      Topic('/robot/init_time',                             Float32))
@nrp.MapSpikeSource(    'LGN',           nrp.map_neurons(range(0, nrp.config.brain_root.nPols*nrp.config.brain_root.nRows*nrp.config.brain_root.nCols), lambda i: nrp.brain.LGN[i]), nrp.dc_source)
@nrp.MapVariable(       'retina',        initial_value= None)
@nrp.MapVariable(       'isBlinking',    initial_value= None,         scope=nrp.GLOBAL)
@nrp.MapVariable(       'eccentricity',  initial_value= None,         scope=nrp.GLOBAL)  # (row, col), right/bottom is positive
@nrp.MapVariable(       'camSpanAngle',  initial_value=[34.31,45.75], scope=nrp.GLOBAL)  # in degrees, also (row, col)
@nrp.MapVariable(       'topLeftCoord',  initial_value= None,         scope=nrp.GLOBAL)
@nrp.Robot2Neuron()
def RET_retina_input(t, image, retina, doCortMagnif, initTime, LGN, inputONToLGN, inputOFFToLGN, isBlinking, eccentricity, camSpanAngle, topLeftCoord):

    # Values in displayStim.exd take time to initialize, so this avoids a bug
    if t > 0.2:

    # Check that the camera is running
        if image.value is not None and t > initTime.value.data:

            # Initialize image
            cameraOutput = CvBridge().imgmsg_to_cv2(image.value)
            imRows       = cameraOutput.shape[0]
            imCols       = cameraOutput.shape[1]

            # Check that the retina is initialized (see below)
            if retina.value is not None:

                # Take the image and feed the retina
                if eccentricity.value is not None:

                    # Parameters and imports
                    import numpy
                    nRows             = nrp.config.brain_root.nRows
                    nCols             = nrp.config.brain_root.nCols
                    retinaToLGNBGain  = 0.02   # 0.02 originally
                    retinaToLGNDGain  = 0.02   # 0.02 originally
                    greyValue         = 127.0  # between 0.0 and 254.0 ; neutral is 127.0

                    # Transform the image input in a retina output
                    retina.value.Step(cameraOutput)                # feed the retina with the camera
                    imgON  = numpy.zeros(cameraOutput.shape[0:2])  # this is bad, I should instead feed the retina with zeros ...!
                    imgOFF = numpy.zeros(cameraOutput.shape[0:2])  # see 4 lines below for an almost working version
                    if not isBlinking.value:
                        imgON  = retina.value.GetOutput('ganglion_bio_ON')
                        imgOFF = retina.value.GetOutput('ganglion_bio_OFF')

                    # The retina can output weird values just after its initialization
                    if (numpy.max(imgON)-numpy.min(imgON)) < 1 or (numpy.max(imgOFF)-numpy.min(imgOFF)) < 1:
                        return

                    # Feed the LGN with the retina input, after checking no weird value is inside
                    if not (numpy.isnan(imgON).any() or numpy.isnan(imgOFF).any()):

                        # Transform the retina input into a cortical input
                        if doCortMagnif.value.data:
                            from specs import transformInput
                            imgON,  centralRow, centralCol = transformInput(imgON,  nRows, nCols, eccentricity.value, camSpanAngle.value)
                            imgOFF, centralRow, centralCol = transformInput(imgOFF, nRows, nCols, eccentricity.value, camSpanAngle.value)
                            firstRow = centralRow-nRows/2
                            firstCol = centralCol-nCols/2
                            topLeftCoord.value = (firstRow, firstCol)  # useful in LAM2_send_signals.py

                        # No cortical magnification
                        else:
                            import numpy
                            eccTan   = [numpy.tan(e*numpy.pi/180.0) for e in eccentricity.value]
                            camTan   = [numpy.tan(c*numpy.pi/180.0) for c in camSpanAngle.value]
                            shift    = [e/c*s/2.0 for e,c,s in zip(eccTan, camTan, imgON.shape)]
                            firstRow = int((imgON.shape[0]-nRows)/2 + shift[0])
                            firstCol = int((imgON.shape[1]-nCols)/2 + shift[1])

                        # Take a rectangular patch around the center + eccentricity
                        inputON  = imgON [firstRow:firstRow+nRows, firstCol:firstCol+nCols]
                        inputOFF = imgOFF[firstRow:firstRow+nRows, firstCol:firstCol+nCols]

                        # Feed the LGN cells with the retina input
                        LGNBrightInput = retinaToLGNBGain*inputON. flatten()
                        LGNDarkInput   = retinaToLGNDGain*inputOFF.flatten()
                        LGN.amplitude  = numpy.hstack((LGNBrightInput, LGNDarkInput))
                        
                        # Display the inputs that are given to the LGN cells (input highlighted with a red square)
                        imgONRGB  = numpy.dstack((imgON,  imgON,  imgON ))
                        imgOFFRGB = numpy.dstack((imgOFF, imgOFF, imgOFF))
                        shp       = imgONRGB.shape
                        displayInputBounds = True
                        if displayInputBounds:
                            imgONRGB [firstRow,                firstCol:firstCol+nCols, :] = [254.0, 0.0, 0.0]
                            imgONRGB [firstRow+nRows,          firstCol:firstCol+nCols, :] = [254.0, 0.0, 0.0]
                            imgONRGB [firstRow:firstRow+nRows, firstCol,                :] = [254.0, 0.0, 0.0]
                            imgONRGB [firstRow:firstRow+nRows, firstCol+nCols,          :] = [254.0, 0.0, 0.0]
                            imgOFFRGB[firstRow,                firstCol:firstCol+nCols, :] = [254.0, 0.0, 0.0]
                            imgOFFRGB[firstRow+nRows,          firstCol:firstCol+nCols, :] = [254.0, 0.0, 0.0]
                            imgOFFRGB[firstRow:firstRow+nRows, firstCol,                :] = [254.0, 0.0, 0.0]
                            imgOFFRGB[firstRow:firstRow+nRows, firstCol+nCols,          :] = [254.0, 0.0, 0.0]
                        messageFrameON  = CvBridge().cv2_to_imgmsg(imgONRGB .astype(numpy.uint8), 'rgb8')
                        messageFrameOFF = CvBridge().cv2_to_imgmsg(imgOFFRGB.astype(numpy.uint8), 'rgb8')
                        inputONToLGN. send_message(messageFrameON )
                        inputOFFToLGN.send_message(messageFrameOFF)

            # Initialize retina if it does not exist yet
            else:

                ################################
                ### DEFINE RETINA PARAMETERS ###
                ################################

                # OPL parameters
                sigma_center   = 0.88
                sigma_surround = 2.35
                tau_center     = 10
                tau_surround   = 10
                n_center       = 2

                # Slow transient
                lambda_OPL     = 100.0
                w_adap         = 0.5
                tau_adap       = 100

                # Amacrine parameters (gain control)
                sigma_amacrine = 2.5
                tau_amacrine   = 5.0
                g_BA           = 5.0
                lambda_BA      = 50.0

                # Ganglion parameters (X_cells)
                w_trs          = 0.7
                tau_trs        = 20
                T0_BG          = 80
                lambda_BG      = 150
                v_BG           = 0
                sigma_ganglion = 0

                # Simulation parameters
                import pyretina
                retina_ = pyretina.Retina()
                retina_.TempStep(1)          # Simulation step (in ms)
                retina_.PixelsPerDegree(0.5) # Pixels per degree of visual angle ; original: 1.0 ; isn't it degree per pixel????
                retina_.DisplayDelay(0)      # Display delay
                retina_.DisplayZoom(1)       # Display zoom
                retina_.DisplayWindows(3)    # Displays per row
                retina_.Input('camera', {'size': (imCols, imRows)})  # Order is correct


                #############################
                ### CREATE RETINA NEURONS ###
                #############################

                # Light cones and OPL (outer plexiform layer)
                # retina_.Create('GaussFilter',        'spatial_center',   {'sigma': sigma_center})
                retina_.Create('SpaceVariantGaussFilter', 'spatial_center', {'sigma': sigma_center, 'K': 0.4, 'R0': 0.0})
                retina_.Create('LinearFilter',       'tmp_center',       {'type': 'Gamma','tau': tau_center,'n': n_center})
                retina_.Create('GaussFilter',        'spatial_surround', {'sigma': sigma_surround})
                retina_.Create('LinearFilter',       'tmp_surround',     {'type': 'Exp', 'tau': tau_surround})
                retina_.Create('Parrot',             'parrot_OPL',       {'gain': lambda_OPL})
                retina_.Create('Parrot',             'parrot_OPL2',      {'gain': 1.0})
                retina_.Create('HighPass',           'highpass_OPL',     {'w': w_adap, 'tau': tau_adap})

                # Amacrine and bipolar
                retina_.Create('GaussFilter',        'spatial_amacrine', {'sigma': sigma_amacrine})
                retina_.Create('LinearFilter',       'tmp_amacrine',     {'type': 'Exp', 'tau': tau_amacrine})
                retina_.Create('StaticNonLinearity', 'SNL_amacrine',     {'slope': lambda_BA, 'offset': g_BA, 'exponent': 2.0})
                retina_.Create('SingleCompartment',  'SC_bipolar',       {'number_current_ports': 1, 'number_conductance_ports': 1, 'Cm': 1.0, 'Rm': 1.0, 'tau': 0.0, 'E': [0.0]})

                # Ganglion ON-cells
                retina_.Create('Parrot',             'parrot_ON',        {'gain': -1.0})
                retina_.Create('HighPass',           'highpass_trs_ON',  {'w': w_trs, 'tau': tau_trs})
                retina_.Create('Rectification',      'rect_trs_ON',      {'T0': T0_BG, 'lambda': lambda_BG, 'V_th': v_BG})
                retina_.Create('GaussFilter',        'spatial_trs_ON',   {'sigma': sigma_ganglion})

                # Ganglion OFF-cells
                retina_.Create('Parrot',             'parrot_OFF',       {'gain': 1.0}) # OFF inverts current
                retina_.Create('HighPass',           'highpass_trs_OFF', {'w': w_trs, 'tau': tau_trs})
                retina_.Create('Rectification',      'rect_trs_OFF',     {'T0': T0_BG, 'lambda': lambda_BG, 'V_th': v_BG})
                retina_.Create('GaussFilter',        'spatial_trs_OFF',  {'sigma': sigma_ganglion})

                # Output of ganglion cells (gains)
                retina_.Create('Parrot',             'parrot_norm1',     {'gain': 1.0/255.0}) # normalize input luminance
                retina_.Create('Parrot',             'parrot_norm2',     {'gain': 1.0/255.0}) # normalize input luminance
                retina_.Create('Parrot',             'ganglion_bio_ON',  {'gain': 0.02*0.0000000001*10**12}) # output in pA
                retina_.Create('Parrot',             'ganglion_bio_OFF', {'gain': 0.02*0.0000000001*10**12}) # otuput in pA


                #################################
                ### DEFINE RETINA CONNECTIONS ###
                #################################

                # Light-cones to OPL (outer plexiform layer)
                retina_.Connect('L_cones',          'parrot_norm1',     'Current')
                retina_.Connect('parrot_norm1',     'spatial_center',   'Current')
                retina_.Connect('spatial_center',   'tmp_center',       'Current')
                retina_.Connect('L_cones',          'parrot_norm2',     'Current')
                retina_.Connect('parrot_norm2',     'spatial_surround', 'Current')
                retina_.Connect('spatial_surround', 'tmp_surround',     'Current')
                retina_.Connect(['spatial_center', '-', 'spatial_surround'], 'parrot_OPL', 'Current')
                retina_.Connect('parrot_OPL',       'highpass_OPL',     'Current')

                # OPL to bipolar cells and amacrine cells
                retina_.Connect('highpass_OPL',     'SC_bipolar',       'Current')
                retina_.Connect('SC_bipolar',       'SNL_amacrine',     'Current')
                retina_.Connect('SNL_amacrine',     'spatial_amacrine', 'Current')
                retina_.Connect('spatial_amacrine', 'tmp_amacrine',     'Current')
                retina_.Connect('tmp_amacrine',     'SC_bipolar',       'Conductance')

                # Bipolar cells to ON ganglion cells
                retina_.Connect('SC_bipolar',       'parrot_ON',        'Current')
                retina_.Connect('parrot_ON',        'highpass_trs_ON',  'Current')
                retina_.Connect('highpass_trs_ON',  'rect_trs_ON',      'Current')
                retina_.Connect('rect_trs_ON',      'spatial_trs_ON',   'Current')
                retina_.Connect('spatial_trs_ON',   'ganglion_bio_ON',  'Current')

                # Bipolar cells to OFF ganglion cells
                retina_.Connect('SC_bipolar',       'parrot_OFF',       'Current')
                retina_.Connect('parrot_OFF',       'highpass_trs_OFF', 'Current')
                retina_.Connect('highpass_trs_OFF', 'rect_trs_OFF',     'Current')
                retina_.Connect('rect_trs_OFF',     'spatial_trs_OFF',  'Current')
                retina_.Connect('spatial_trs_OFF',  'ganglion_bio_OFF', 'Current')


                #################################
                ### CREATE AND DISPLAY RETINA ###
                #################################

                retina.value = retina_
                retina_.Show('ganglion_bio_ON',  True, {'margin': 0})
                retina_.Show('ganglion_bio_OFF', True, {'margin': 0})
