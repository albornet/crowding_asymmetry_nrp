# Imported python transfer function that plots the activity of a layer of the model
import numpy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
@nrp.MapRobotPublisher('V2Activity',    Topic('/robot/V2Activity', Image))
@nrp.MapSpikeSink(     'V2LayerToPlot', nrp.brain.V2Layer23Seg, nrp.spike_recorder)
@nrp.MapVariable(      'plotSegSignal', scope=nrp.GLOBAL, initial_value=numpy.zeros((nrp.config.brain_root.nSegLayers-1, nrp.config.brain_root.nRows, nrp.config.brain_root.nCols)))
@nrp.Neuron2Robot()
def LAM2_plot_V2(t, V2LayerToPlot, V2Activity, plotSegSignal):

    # Load parameters and libraries
    import numpy
    min_idx             = int(V2LayerToPlot.neurons[0])
    nSegLayers          = nrp.config.brain_root.nSegLayers
    nOris               = nrp.config.brain_root.nOri
    nRows               = nrp.config.brain_root.oriRows
    nCols               = nrp.config.brain_root.oriCols
    nNeuronsPerSegLayer = nOris*nRows*nCols

    # Build the message to send from the spike recorder
    plotDensityV2  = numpy.zeros(nSegLayers*nNeuronsPerSegLayer)
    msg_to_send    = 254.0*numpy.ones((nSegLayers*(nRows+1)-1,nCols,3), dtype=numpy.uint8)
    for (idx, time) in V2LayerToPlot.times.tolist():
        plotDensityV2[int(idx)-min_idx] = plotDensityV2[int(idx)-min_idx] + 1  # +1 spike to the event position

    # Go through all segmentation layers and build the map to plot
    for h in range(nSegLayers):

        # Detect the spikes in each segmentation layer
        segLayerPlot = plotDensityV2[h*nNeuronsPerSegLayer:(h+1)*nNeuronsPerSegLayer]
        segLayerPlot = numpy.reshape(segLayerPlot, (nOris,nRows,nCols))
        if numpy.any(segLayerPlot):
            segLayerPlot = segLayerPlot*254.0/10.0  # max rate is 1 spike every 2 ms = 10 spikes per TF step (20 ms)

        # Set up a color map for the gif, following the number of orientations
        if nOris==2:        # Vertical and horizontal
            rgbMap = numpy.array([[1.,0.,0.], [0.,1.,0.]])
        if nOris==4:        # Vertical, horizontal, both diagonals
            rgbMap = numpy.array([[0.,0.,1.], [1.,0.,0.], [0.,0.,1.], [0.,1.,0.]])
        if nOris==8:
            rgbMap = numpy.array([[0.,.5,.5], [0.,0.,1.], [.5,0.,.5], [1.,0.,0.], [.5,0.,.5], [0.,0.,1.], [0.,.5,.5], [0.,1.,0.]])
        
        # Build a couloured image according the spike rate of each neuron
        dataV2 = numpy.tensordot(segLayerPlot[:,:,:], rgbMap, axes=(0,0))

        # Highlight the segmentation signal on the plot
        if h > 0:
            dataV2[:-1,:-1,2] = dataV2[:-1,:-1,2] + numpy.minimum(254, dataV2[:-1,:-1,2] + plotSegSignal.value[h-1,:,:])

        # Publish the V2 acticity map for this segmentation layer
        msg_to_send[h*(nRows+1):(h+1)*(nRows+1)-1,:,:] = dataV2

    # Send the message to display
    msg_frame = CvBridge().cv2_to_imgmsg(msg_to_send.astype(numpy.uint8), 'rgb8')
    V2Activity.send_message(msg_frame)
