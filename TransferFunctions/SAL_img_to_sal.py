# Imported python transfer function that transforms the output of any saliency model into an array 
from sensor_msgs.msg import Image
from std_msgs.msg import Bool
@nrp.MapRobotSubscriber('camera',       Topic('/robot/icub_model/right_eye_camera/image_raw', Image  ))
@nrp.MapRobotPublisher( 'saliency_map', Topic('/robot/saliency_map',                          Image  ))
@nrp.MapRobotSubscriber('doCortMagnif', Topic('/robot/do_cort_m',                             Bool   ))
@nrp.MapRobotSubscriber('initTime',     Topic('/robot/init_time',                             Float32))
@nrp.MapVariable(       'tf_session',   initial_value=None)
@nrp.MapVariable(       'tf_network',   initial_value=None)
@nrp.MapVariable(       'modelPath',    initial_value=None)
@nrp.MapVariable(       'model',        initial_value=None)
@nrp.MapVariable(       'eccentricity', initial_value=None,          scope=nrp.GLOBAL)  # (row, col), right/bottom is positive
@nrp.MapVariable(       'camSpanAngle', initial_value=[34.31,45.75], scope=nrp.GLOBAL)  # in degrees, also (row, col)
@nrp.Robot2Neuron()
def SAL_img_to_sal(t, camera, saliency_map, initTime, doCortMagnif, tf_session, tf_network, modelPath, model, eccentricity, camSpanAngle):

    ######################################################################################
    ### Works with tensorflow-gpu==1.12.0, protobuf==3.5.2, cuda==9.0 and cudnn==7.4.2 ###
    ######################################################################################

    # Make tensorflow available from home directory installation
    import numpy as np, site, os, cv2
    # site.addsitedir(os.path.expanduser('~/.opt/platform_venv/lib/python2.7/site-packages'))
    site.addsitedir(os.path.expanduser('~/.opt/tensorflow_venv/lib/python2.7/site-packages'))
    import tensorflow as tf
    from cv_bridge import CvBridge
    newModelPath = os.environ['HBP']+'/Models/brain_model/saliency/model.ckpt'
    # The models below work only if you have them!!
    # newModelPath = '/Your/Path/DeepGazeII.ckpt'
    # newModelPath = '/Your/Path/ICF.ckpt'
    config = tf.ConfigProto()
    # config.gpu_options.allow_growth = True
    config.gpu_options.per_process_gpu_memory_fraction = 0.4

    # Import the saliency model
    if modelPath.value != newModelPath:

        # Build the tensorflow session and restore the model from path
        modelPath.value  = newModelPath
        tf_session.value = tf.Session(config=config)
        tf_network.value = tf.train.import_meta_graph(modelPath.value+'.meta')
        tf_network.value.restore(tf_session.value, modelPath.value)

        # Set the model information (input and output indexes) according its name
        if 'DeepGazeII' in modelPath.value:
            model.value = {
                'name' :  'DeepGazeII',
                'in'   : [[tf.get_collection('input_tensor')[0],[]], [tf.get_collection('centerbias_tensor')[0],[]]],
                'out'  :   tf.get_collection('log_density')[0],
                'scale':   5000}
        elif 'ICF' in modelPath.value:
            model.value = {
                'name' :  'ICF',
                'in'   : [[tf.get_collection('input_tensor')[0],[]], [tf.get_collection('centerbias_tensor')[0],[]]],
                'out'  :   tf.get_collection('log_density')[0],
                'scale':   50000}
        else:
            graph       = tf.get_default_graph()
            model.value = {
                'name' :  'Kroner',
                'in'   : [[graph.get_tensor_by_name(   'Placeholder_1:0' ),[]]],
                'out'  :   graph.get_operation_by_name('conv2d_8/BiasAdd').outputs[0]}
            
    # Use the saliency model
    elif camera.value is not None and eccentricity.value is not None and t > initTime.value.data:

        # Collect the input image and what has to be fed to the network
        from scipy.misc import imresize
        image     = CvBridge().imgmsg_to_cv2(camera.value,'rgb8')
        feed_list = model.value['in']

        # Collect the saliency model's output for Alexander Kroner's network
        if model.value['name'] == 'Kroner':
            feed_list[0][1] = np.expand_dims(np.transpose(image,(2,0,1)),axis=0)
            saliency = tf_session.value.run(model.value['out'],feed_dict=dict(feed_list))[0,0,:,:]
            saliency = 255*(saliency-np.min(saliency))

        # Collect the saliency model's output for DeepGazeII or ICF
        if model.value['name'] in ['DeepGazeII', 'ICF']:
            feed_list[0][1] = np.expand_dims(image,axis=0)
            feed_list[1][1] = np.zeros((1,image.shape[0],image.shape[1],1))
            saliency = tf_session.value.run(model.value['out'],feed_dict=dict(feed_list))[0,:,:,0]
            saliency = 255*np.exp(saliency)*model.value['scale']
            saliency[saliency>255] = 255

        # Publish the output as an image
        saliency  = np.dstack((saliency,saliency,saliency))
        if 0:  # doCortMagnif.value.data:
            from specs import transformInput
            nRows = nrp.config.brain_root.nRows
            nCols = nrp.config.brain_root.nCols
            saliency, centralRow, centralCol = transformInput(saliency, nRows, nCols, eccentricity.value, camSpanAngle.value)
        msg_frame = CvBridge().cv2_to_imgmsg(saliency.astype(np.uint8),'rgb8')
        saliency_map.send_message(msg_frame)
