# ! /usr/bin/env python
# PyNN implementation of the LAMINART model of the visual cortex.

from hbp_nrp_cle.brainsim import simulator as sim
import numpy as np
from specs import MyConnector, createConvFilters, createGroupingFilters


########################################
### All parameters of the simulation ###
########################################


# Main parameters (the following sections are secondary parameters)
resolution          =    1.0      # time step of the computation of the neurons dynamical equations
nThreadsToUse       =    7        # define here how many cores you want to use (1 for normal for-loop)
sim.setup(timestep=resolution, min_delay=1.0, max_delay=2.0, threads=nThreadsToUse)  # why not max_delay = 1.0?

# General parameters
nRows               =   50        # heigth of the Laminart model's input
nCols               =   50        # width  of the Laminart model's input
weightScale         =    0.001    # general weight for all connections between neurons !!!PyNN vs. NEST standard!!!

# Oriented filters parameters
nOri                =    8        # number of orientations
convSize            =    6        # size of the oriented convolutionnal filters from LGN to V1 [pixels]
phi                 =    0        # phase for the detectable orientations [rad]
sigmaX              =    0.5      # how much the receptive fields expand across the orientation direction [pixels]
sigmaY              =    convSize # how much the receptive fields expand along  the orientation direction [pixels]
oLambda             =    4.0      # general size of the receptive field [pixels]
V1_SurroundRange    =    2        # 3 originally but I lowered it to avoid too complex interactions in complex shapes
V2_SurroundRange    =    2        # 3 originally but I lowered it to avoid too complex interactions in complex shapes

# Grouping mechanism parameters
dampingInput        =  200.0      # this input will block the boundary spreading control for a certain amount of time [pA]
boundaryGroupSpeeds =   [2]       # [1,5] ; in pixel(s) per grouping step CHANGE
groupTrigSegDelay   =   30.0      # 100.0  # time [ms] between the damping delay turn off and the segmentation signal turn on

# Segmentation parameters
nSegLayers          =    2        # number of segmentation layers ; each segmentation layer represents a group in the visual field
useSegmentation     =    1        # set to 0 if you want to have several segmentation layers, but not to send any segmentation signal
segSignalSize       =   12        # on the cortical map
segSignalAmplitude  =    1.0      # gain for the segmentation signals ; in pA (?)
segSignalDuration   =   50.0      # how long the segmentation signal lasts ; in ms
boundarySegSpeeds   =   [1]       # Could try with more... but segmentation is already fast ; in pixel(s) per spreading step

# Filling-in parameters
V4SpreadingRange    =    1        # V4 cells activity spontaneously spread
nPols               =    2        # simple bright or dark index for V4 and LGN cells

# Neurons parameters (correspond to NEST default parameters)
cellParams = {
    'i_offset'   :   0.0,  # (nA)
    'tau_m'      :  10.0,  # (ms)
    'tau_syn_E'  :   2.0,  # (ms)
    'tau_syn_I'  :   2.0,  # (ms)
    'tau_refrac' :   2.0,  # (ms)
    'v_rest'     : -70.0,  # (mV)
    'v_reset'    : -70.0,  # (mV)
    'v_thresh'   : -55.0,  # (mV)
    'cm'         :   0.25} # (nF)
cellType = sim.IF_curr_alpha(**cellParams)

# Connection parameters
connections = {
    
    # LGN
    'LGN_ToV1Excite'        :   1500.0,  #   1500

    # V1 classic
    'V1_ComplexExcite'      :   1500.0,  #   1500
    'V1_SurroundInhib'      :   -200.0,  #   -200
    'V1_CrossOriInhib'      :  -2000.0,  #  -2000
    'V1_ToV2Excite'         :   5000.0,  #   5000

    # V2 classic
    'V2_ComplexExcite'      :   2000.0,  #   2000
    'V2_SurroundInhib'      :    -50.0,  #    -50
    'V2_CrossOriInhib'      :  -2000.0,  #  -2000
    'V2_SegmentExcite'      :   5000.0,  #   2000
    'V2_SegmentInhib'       : -50000.0,  # -10000

    # V2 grouping
    'V2_SpreadingExcite'    :   1450.0,  #   1450
    'V2_ControlExcite'      :   2400.0,  #   2400
    'V2_ControlInhib'       :  -2000.0,  #  -2000
    'V2_ToV1FeedbackExcite' :    300.0,  #    300

    # Boundary segmentation layers
    'B_SegmentSpreadExcite' :    800.0,  #    800
    'B_ControlSpreadExcite' :   2000.0,  #   2000
    'B_InterNeuronsInhib'   :  -2300.0,  #  -2000  !!
    'B_TriggerV2Inhib'      : -10000.0,  # -10000
    'B_FeedbackV2Inhib'     : -10000.0,  # -10000
    'B_SendSignalExcite'    :    500.0,  #    500
    'B_ResetSignalInhib'    :  -1000.0,  #  -1000

    # V4 filling-in
    'V4_Competition'        :  -5000.0,  #  -5000
    'V4_Spreading'          :   1000.0,  #   1000
    'V2_TriggerSpreading'   :   1000.0,  #   1000
    'V2_StopSpreading'      :   -400.0,  #   -400
    'V4_SegExcite'          :   1000.0,  #   1000

    # Reset signal (for blinks)
    'ResetInhib'            : -50000.0}

# Scale the weights, if needed
for key, value in connections.items():
    connections[key] = value*weightScale


################################################
### Input dimensions and orientation filters ###
################################################


# Read the image from Stimuli/ and create boundary coordinates (between pixels coordinates)
oriRows      = nRows + 1                              # Oriented contrasts map is in between regular pixels
oriCols      = nCols + 1                              # Oriented contrasts map is in between regular pixels
oppOri       = list(np.roll(range(nOri), nOri/2))     # Indexes for orthogonal orientations
convFilters  = createConvFilters(nOri, convSize, phi, sigmaX, sigmaY, oLambda)
groupFilters = createGroupingFilters(nOri, boundaryGroupSpeeds, phi)


#################################################
### Create the neuron layers (LGN, V1, V2, V4 ###
#################################################


# LGN
LGN          = sim.Population(nPols*1*nRows*nCols, cellType, label='LGN')

# Areas V1
V1Layer4     = sim.Population(nPols*nOri*oriRows*oriCols, cellType, label='V1Layer4')
V1Layer23    = sim.Population(1*nOri*oriRows*oriCols, cellType, label='V1Layer23')

# Aera V2
V2Layer4     = sim.Population(1*nOri*oriRows*oriCols, cellType, label='V2Layer4')
V2Layer23    = sim.Population(1*nOri*oriRows*oriCols, cellType, label='V2Layer23')

# Area V2 grouping
V2Layer23Int = sim.Population(1*nOri*oriRows*oriCols, cellType, label='V2Layer23Int')

# Boundary segmentation
V2Layer23Seg = sim.Population(nSegLayers*nOri*oriRows*oriCols, cellType, label='V2Layer23Seg')
if nSegLayers > 1:
    V2SegNet = sim.Population((nSegLayers-1)*1*oriRows*oriCols, cellType, label='V2SegNet')
    V2SegInt = sim.Population((nSegLayers-1)*1*oriRows*oriCols, cellType, label='V2SegInt')

# Area V4
V4           = sim.Population(nPols*1*nRows*nCols, cellType, label='V4')

# Reset cell (for sacadic inhibition and to reset segmentation)
resetNeuron  = sim.Population(1*1*1*1, cellType, label='resetNeuron')


#######################################################################
###  Neurons layers are defined, now set up connexions between them ###
#######################################################################


############ Area V1 ############

# LGN cells project to V1, following feature-based convolutional filters
for p in range(nPols):                                               # Polarities
    for k in range(nOri):                                            # Orientations
        for i2 in range(-convSize/2, convSize/2):                    # Filter rows
            for j2 in range(-convSize/2, convSize/2):                # Filter columns
                source  = []
                target  = []
                source2 = []
                target2 = []
                for i in range(convSize/2, oriRows-convSize/2):      # Rows
                    for j in range(convSize/2, oriCols-convSize/2):  # Columns
                        if i+i2 >=0 and i+i2<nRows and j+j2>=0 and j+j2<nCols:

                            # Connections from LGN to oriented polarized V1 cells, using convolutionnal filters
                            if abs(convFilters[p][k][i2+convSize/2][j2+convSize/2]) > 0.1:
                                source. append(   p *       nRows*  nCols +                    (i+i2)*  nCols + (j+j2))
                                target. append(   p *nOri*oriRows*oriCols + k*oriRows*oriCols + i    *oriCols +  j    )
                                source2.append(   p *       nRows*  nCols +                    (i+i2)*  nCols + (j+j2))
                                target2.append((1-p)*nOri*oriRows*oriCols + k*oriRows*oriCols + i    *oriCols +  j    )

                # Define the weight for this filter position and orientation
                polarWeight1 = connections['LGN_ToV1Excite']*convFilters[  p][k][i2+convSize/2][j2+convSize/2]
                polarWeight2 = connections['LGN_ToV1Excite']*convFilters[1-p][k][i2+convSize/2][j2+convSize/2]

                # LGN -> Layer 4 (simple cells) connections (no connections at the edges, to avoid edge-effects)
                sim.Projection(LGN, V1Layer4, MyConnector(source,  target ), sim.StaticSynapse(weight=polarWeight1))
                sim.Projection(LGN, V1Layer4, MyConnector(source2, target2), sim.StaticSynapse(weight=polarWeight2))

# Convergent excitatory connection from both polarities of V1Layer4 to V1Layer23
source = []
target = []  # DO IT AS A ONE LINER
for p in range(nPols):
    for k in range(nOri):
        for i in range(oriRows):
            for j in range(oriCols):

                source.append(p*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                target.append(                         k*oriRows*oriCols + i*oriCols + j)

sim.Projection(V1Layer4,  V1Layer23, MyConnector(source, target), sim.StaticSynapse(weight=connections['V1_ComplexExcite']))


############ Area V2 ############

# V1 projection to V2 and further to layers 23
sim.Projection(V1Layer23, V2Layer4,  sim.OneToOneConnector(), sim.StaticSynapse(weight=connections['V1_ToV2Excite'   ]))
sim.Projection(V2Layer4,  V2Layer23, sim.OneToOneConnector(), sim.StaticSynapse(weight=connections['V2_ComplexExcite']))

# Projection from V2Layer23 to the segmentation cells
source  = []
source2 = []
source3 = []
target  = []
target2 = []
target3 = []
for k in range(nOri):
    for i in range(oriRows):
        for j in range(oriCols):
            for h in range(nSegLayers):
                source.append(                         k*oriRows*oriCols + i*oriCols + j)
                target.append(h*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

                # Boundary inhibition between segmentation layers (upward, stronger)
                for h2 in range(h, nSegLayers-1):
                    source2.append( h    *nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                    target2.append((h2+1)*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

                # Boundary inhibition between segmentation layers (downward, weaker)
                if h!=0:
                    source3.append(h*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                    target3.append(0*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

# Projection from V2Layer23 to the segmentation cells and inhibition between segmentation layers
sim.Projection(V2Layer23,    V2Layer23Seg, MyConnector(source , target ), sim.StaticSynapse(weight=connections['V2_SegmentExcite']     ))
sim.Projection(V2Layer23Seg, V2Layer23Seg, MyConnector(source2, target2), sim.StaticSynapse(weight=connections['V2_SegmentInhib' ]     ))
# sim.Projection(V2Layer23Seg, V2Layer23Seg, MyConnector(source3, target3), sim.StaticSynapse(weight=connections['V2_SegmentInhib' ]*0.5))  # 0.5


############ V1/V2 competition stages ############

# Two separated consecutive competition stages: surround and cross-orientation inhibition
VSurroundRanges = [V1_SurroundRange, V2_SurroundRange]
VSurroundInhibs = [connections['V1_SurroundInhib'], connections['V2_SurroundInhib']]
for k in range(nOri):       # Source orientation
    for k2 in range(nOri):  # Target orientation

        # Different weights that correspond to the level of colinearity and orthogonality of two orientations
        colinOriWeight = 1.0/(1.0 + 8.0/nOri*min((k-       k2 )%nOri, (       k2 -k)%nOri))  # how colinear   k and k2 are
        orthoOriWeight = 1.0/(1.0 + 8.0/nOri*min((k-oppOri[k2])%nOri, (oppOri[k2]-k)%nOri))  # how orthogonal k and k2 are

        # Close-range inhibition between similar orientation
        if colinOriWeight**2 > 0.1:
            for V1_or_V2 in [0,1]:
                VRange = VSurroundRanges[V1_or_V2]
                for i2 in range(-VRange, VRange+1):       # Filter rows
                    for j2 in range(-VRange, VRange+1):   # Filter columns
                        if i2!=0 or j2!=0:

                            source  = []
                            target  = []
                            for i in range(oriRows):      # Rows
                                for j in range(oriCols):  # Columns
                                    if 0 <= i+i2 < oriRows and 0 <= j+j2 < oriCols:

                                        source.append(k *oriRows*oriCols +  i    *oriCols +  j    )
                                        target.append(k2*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                            surroundWeight = colinOriWeight*VSurroundInhibs[V1_or_V2]/np.sqrt(i2**2+j2**2)
                            if V1_or_V2 == 0: 
                                sim.Projection(V1Layer23, V2Layer4,  MyConnector(source, target), sim.StaticSynapse(weight=surroundWeight))
                            if V1_or_V2 == 1:
                                sim.Projection(V2Layer4,  V2Layer23, MyConnector(source, target), sim.StaticSynapse(weight=surroundWeight))

        # Local inhibition between different orientations
        if orthoOriWeight**2 > 0.1:

            source  = []
            target  = []
            for i in range(oriRows):      # Rows
                for j in range(oriCols):  # Columns

                        source.append(k *oriRows*oriCols + i*oriCols + j)
                        target.append(k2*oriRows*oriCols + i*oriCols + j)

            crossOriWeight = orthoOriWeight*connections['V1_CrossOriInhib']
            sim.Projection(V2Layer4,  V2Layer4,  MyConnector(source, target), sim.StaticSynapse(weight=crossOriWeight))
            sim.Projection(V2Layer23, V2Layer23, MyConnector(source, target), sim.StaticSynapse(weight=crossOriWeight))


############ Grouping mechanisms ############

# Multi-scale illusory contours dynamics
for side in range(2):                            # Grouping either on one side or the other
    for h in range(len(boundaryGroupSpeeds)):    # Different ranges of spreading
        center = boundaryGroupSpeeds[h]          # Center of the grouping filter
        for i2 in range(-center, center+1):      # Filter rows
            for j2 in range(-center, center+1):  # Filter columns
                for k in range(nOri):            # Source orientations
                    for k2 in range(nOri):       # Target orientations

                        # Grouping connections on each side ; the connection weights are taken from the grouping filters
                        groupingWeight = connections['V2_SpreadingExcite']*groupFilters[side][h][k][k2][i2+center][j2+center]
                        controlWeight  = connections['V2_ControlExcite'  ]*groupFilters[side][h][k][k2][i2+center][j2+center]
                        if groupingWeight > 0:

                            source  = []
                            target  = []
                            for i in range(oriRows):      # Rows
                                for j in range(oriCols):  # Columns
                                    if i+i2 >= 0 and i+i2 < oriRows and j+j2 >= 0 and j+j2 < oriCols:

                                        # Connections among V2Layer23 cells and bipole circuit
                                        target.append(k *oriRows*oriCols +  i    *oriCols +  j    )
                                        source.append(k2*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                            # V2Layer23 spreads to neighbors and excites or inhibits interneurons on each side
                            sim.Projection(V2Layer23, V2Layer23,    MyConnector(source, target), sim.StaticSynapse(weight=groupingWeight))
                            sim.Projection(V2Layer23, V2Layer23Int, MyConnector(source, target), sim.StaticSynapse(weight=controlWeight ))

# # Control and damping (orientation specific)
# source  = []
# target  = []
# for k in range(nOri):             # Orientations
#     for i in range(oriRows):      # Rows
#         for j in range(oriCols):  # Columns
#             source.append(k                                )
#             target.append(k*oriRows*oriCols + i*oriCols + j)

# The control layer does not let the spreading go, but the damping allows it direction-specifically (and illusory contours may stabilize)
sim.Projection(V2Layer23Int, V2Layer23,    sim.OneToOneConnector(),     sim.StaticSynapse(weight=connections['V2_ControlInhib']))
sim.Projection(V2Layer23,    V2Layer23Int, sim.OneToOneConnector(),     sim.StaticSynapse(weight=connections['V2_ControlInhib']))
# sim.Projection(V2Layer23Grp, V2Layer23Int, MyConnector(source, target), sim.StaticSynapse(weight=connections['V2_ControlInhib']))

# V2 is fed back to V1 (espescially the illusory contours)
sim.Projection(V2Layer23, V1Layer23, sim.OneToOneConnector(), sim.StaticSynapse(weight=connections['V2_ToV1FeedbackExcite']))


########### Boundary segmentation network ###################

# Segmentation dynamics
if nSegLayers > 1:

    source  = []
    source2 = []
    source3 = []
    source4 = []
    source5 = []
    target  = []
    target2 = []
    target3 = []
    target4 = []
    target5 = []
    for h in range(nSegLayers-1):                               # Segmentation layers (not including baseline layer)
        for i in range(oriRows):                                # Rows
            for j in range(oriCols):                            # Columns
                for sIndex, s in enumerate(boundarySegSpeeds):  # TEST

                    # All spreading interactions
                    for i2 in range(-(s+1), (s+1)+1):
                        for j2 in range(-(s+1), (s+1)+1):
                            if (i2 != 0 or j2 != 0) and 0 <= i+i2 < oriRows and 0 <= j+j2 < oriCols:

                                # Segmentation spreads by itself
                                if abs(i2) <= s and abs(j2) <= s:
                                    source.append(h*oriRows*oriCols +  i    *oriCols +  j    )
                                    target.append(h*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                                # Segmentation spread control (further than segmentation spread itself)
                                source2.append(h*oriRows*oriCols +  i    *oriCols +  j    )
                                target2.append(h*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                    # All non-spreading interactions
                    for k in range(nOri):

                        # Segmentation network triggers activity in its corresponding segmentation layer in V2
                        for h2 in range(h+1):
                            source3.append(h   *                         oriRows*oriCols + i*oriCols + j)
                            target3.append(h2  *nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)

                        # V2 (corresponding segmentation layer) triggers activity in the segmentation network
                        source4.append((h+1)*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                        target4.append( h   *                         oriRows*oriCols + i*oriCols + j)

                        # V1 triggers spreading in the segmentation network (orientation specific)
                        source5.append(                             k*oriRows*oriCols + i*oriCols + j)
                        target5.append( h   *                         oriRows*oriCols + i*oriCols + j)

    # Spreading, spreading control, and triggering (segmentation network disinhibit contours representation in its own segmentation layer in V2)
    sim.Projection(V2SegInt, V2SegNet,     sim.OneToOneConnector(),       sim.StaticSynapse(weight=connections['B_InterNeuronsInhib'  ]))
    sim.Projection(V2SegNet, V2SegNet,     MyConnector(source , target ), sim.StaticSynapse(weight=connections['B_SegmentSpreadExcite']))
    sim.Projection(V2SegNet, V2SegInt,     MyConnector(source2, target2), sim.StaticSynapse(weight=connections['B_ControlSpreadExcite']))
    sim.Projection(V2SegNet, V2Layer23Seg, MyConnector(source3, target3), sim.StaticSynapse(weight=connections['B_TriggerV2Inhib'     ]))

    # V2 segmentation layer is fed back to the spreading control to stabilize its activity ; also, V2Layer23 pre-inhibit the spreading control
    sim.Projection(V2Layer23Seg, V2SegInt, MyConnector(source4, target4), sim.StaticSynapse(weight=connections['B_FeedbackV2Inhib']))
    sim.Projection(V2Layer23,    V2SegInt, MyConnector(source5, target5), sim.StaticSynapse(weight=connections['B_FeedbackV2Inhib']))


############## Area V4 ################

source  = []
source2 = []
source3 = []
source4 = []
source5 = []
target  = []
target2 = []
target3 = []
target4 = []
target5 = []
for i in range(nRows):
    for j in range(nCols):

        # V4 signals spread in any direction
        for i2 in [-V4SpreadingRange, V4SpreadingRange]:       # [-V4SpreadingRange, 0, V4SpreadingRange]
            for j2 in [-V4SpreadingRange, V4SpreadingRange]:   # [-V4SpreadingRange, 0, V4SpreadingRange]
                if i+i2 >=0 and i+i2<nRows and j+j2>=0 and j+j2<nCols:
                    for p in range(nPols):                     # Polarities
                    
                        # Spread in the normal V4 layer
                        source.append(p*nRows*nCols + i    *nCols +  j    )
                        target.append(p*nRows*nCols +(i+i2)*nCols + (j+j2))

        # V2 cells inhibit filling-in and V1 cells trigger filling-in in specific directions
        for k in range(nOri):                                  # Orientations
            filterSize = convFilters[0][k].shape[0]            # Size of the filter to span
            for i2 in range(-filterSize/2, filterSize/2):      # Filter rows
                for j2 in range(-filterSize/2, filterSize/2):  # Filter columns
                    if i+i2 >=0 and i+i2<nRows and j+j2>=0 and j+j2<nCols:
                        for p in range(nPols):                 # Polarities

                            # Connection from V2Layer23 to V4 cells, on both sides
                            if abs(convFilters[0][k][i2+filterSize/2][j2+filterSize/2]) > 0.1:  # any([:]? more elegant)
                                source2.append(k*oriRows*oriCols +  i    *oriCols +  j    )
                                target2.append(p*  nRows*  nCols + (i+i2)*  nCols + (j+j2))

                            # Connection from V1Layer4 to V4 cells, in the orthogonal direction as the V1 orientation (1st side)
                            if convFilters[1][k][i2+filterSize/2][j2+filterSize/2] > 0.1:
                                source3.append(   p *nOri*oriRows*oriCols + k*oriRows*oriCols + i    *oriCols +  j    )
                                target3.append(   p *       nRows*  nCols +                    (i+i2)*  nCols + (j+j2))
                        
                            # Connection from V1Layer4 to V4 cells, in the orthogonal direction as the V1 orientation (2nd side)
                            if convFilters[0][k][i2+filterSize/2][j2+filterSize/2] > 0.1:
                                source4.append(   p *nOri*oriRows*oriCols + k*oriRows*oriCols + i    *oriCols +  j    )
                                target4.append((1-p)*       nRows*  nCols +                    (i+i2)*  nCols + (j+j2))

        # V4 polarities compete (bright vs dark)
        for p in range(nPols):
            source5.append(   p *nRows*nCols + i*nCols + j)
            target5.append((1-p)*nRows*nCols + i*nCols + j)

# V4 activity spreads in any direction
sim.Projection(V4,       V4, MyConnector(source, target),   sim.StaticSynapse(weight=connections['V4_Spreading']))

# V2 non-polar activity stops spreading, so that filling-in stops at the shapes boundaries
sim.Projection(V2Layer4, V4, MyConnector(source2, target2), sim.StaticSynapse(weight=connections['V2_StopSpreading']))

# V2 polar activity triggers spreading directionnally, so that the shape brightness is on the correct side
sim.Projection(V1Layer4, V4, MyConnector(source3, target3), sim.StaticSynapse(weight=connections['V2_TriggerSpreading']))
sim.Projection(V1Layer4, V4, MyConnector(source4, target4), sim.StaticSynapse(weight=connections['V2_TriggerSpreading']))

# V4 brightness and darkness signals compete between each other
sim.Projection(V4,       V4, MyConnector(source5, target5), sim.StaticSynapse(weight=connections['V4_Competition']))


############## Reset ################

sim.Projection(resetNeuron, LGN,          sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
sim.Projection(resetNeuron, V1Layer4,     sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
sim.Projection(resetNeuron, V1Layer23,    sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
sim.Projection(resetNeuron, V2Layer4,     sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
sim.Projection(resetNeuron, V2Layer23,    sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
sim.Projection(resetNeuron, V2Layer23Int, sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
sim.Projection(resetNeuron, V2Layer23Seg, sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
sim.Projection(resetNeuron, V4,           sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
if nSegLayers > 1:
    sim.Projection(resetNeuron, V2SegNet, sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
    sim.Projection(resetNeuron, V2SegInt, sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
    