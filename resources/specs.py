###############################
### Some useful definitions ###
###############################


# Imports
import numpy as np
import nest
import re
from pyNN.connectors import Connector


# Create a custom connector (use sim.Projection explicitly to go faster)
class MyConnector(Connector):
    def __init__(self, source, target):
        self.source = source
        self.target = target
    def connect(self, projection):
        nest.Connect([projection.pre.all_cells[s] for s in self.source], [projection.post.all_cells[t] for t in self.target], 'one_to_one', syn_spec=projection.nest_synapse_model)


# Create different filters for orientation selectivity
def createConvFilters(nOri, size, phi, sigmaX, sigmaY, oLambda):

    # Initialize the filters
    filters = np.zeros((nOri, size, size))

    # Fill them with gabors
    midSize = (size-1.)/2.0
    maxValue = -1
    for k in range(0, nOri):
        theta = np.pi*(k+1)/nOri + phi
        for i in range(0, size):
            for j in range(0, size):
                x = (i-midSize)*np.cos(theta) + (j-midSize)*np.sin(theta)
                y = -(i-midSize)*np.sin(theta) + (j-midSize)*np.cos(theta)
                filters[k][i][j] = np.exp(-((x*x)/sigmaX + (y*y)/sigmaY)) * np.sin(2*np.pi*x/oLambda)

    # Normalize the orientation filters so that they have the same overall strength
    for k in range(nOri):
        filters[k] /= np.sqrt(np.sum(filters[k]**2))

    # Return the filters to the computer
    return (filters, -filters)


# Set up filters for boundary grouping spreads (illusory contours)
def createGroupingFilters(numOrientations, boundaryGroupingSpeeds, phi):

    # Set up filters for Boundary grouping (nearest neighbors); there are 2 filters, one for grouping in one direction and another for the other direction
    boundaryGroupFilter = [[] for s in range(2)]
    for side in range(2):                             # Grouping either or one side or on the other side
        sideValue = int((-2)*(side-0.5))              # Useful sign (1 for side = 0, -1 for side = 1)
        for h in range(len(boundaryGroupingSpeeds)):  # Grouping at different ranges

            # Identify center of filter
            center_xy = boundaryGroupingSpeeds[h]
            boundaryGroupFilter[side].append(np.zeros((numOrientations, numOrientations, 2*center_xy+1, 2*center_xy+1)))
            for k in range(numOrientations):          # Source orientation

                thetaSource = np.pi*(k+1)/numOrientations + phi
                if numOrientations == 8 and k in [0,2,4,6]:
                    threshold = 0.5
                else:
                    threshold = 1.0
                for k2 in [k]: # range(numOrientations):     # Target orientation

                    thetaTarget      = np.pi*(k2+1)/numOrientations + phi          # Originally np.pi*(k2)/numOrientations
                    orientationMatch = np.abs(np.cos(thetaSource-thetaTarget))  # Match of source and target orientations
                    for i in range(2*boundaryGroupingSpeeds[h]+1):
                        for j in range(2*boundaryGroupingSpeeds[h]+1):

                            thetaPixel = np.arctan2(-(i-center_xy), (j-center_xy))           # /!\ "-i" over "j" (trigonometry: "-y" over "x")
                            orientationMatch2 = np.abs( np.cos( thetaSource-thetaPixel))  # source orientation vs neighbor position
                            orientationMatch3 = np.abs( np.cos( thetaTarget-thetaPixel))  # target orientation vs neighbor position
                            orientationMatch4 = np.sin(-np.pi/2+thetaTarget-thetaPixel )  # target orientation vs neighbor position, to make directional filter
                            value = orientationMatch * orientationMatch2 * (orientationMatch3**3) * orientationMatch4

                            # Insert the value in the corresponding grouping filter
                            if sideValue*value >= threshold and not (i==center_xy and j==center_xy):    # put back 1.0!!!
                                distanceToCenter = np.sqrt((i-center_xy)**2 + (j-center_xy)**2)
                                boundaryGroupFilter[side][h][k][k2][i][j] = sideValue*value**3/distanceToCenter

    # Return the filters to the computer
    return boundaryGroupFilter


# Create a cartesian to cortical conversion array
def cart2cort(nRows, nCols, nRowsT, nColsT):

    # Parameters
    a   =    1.0  # shift
    hX  =    5.0  # x-slope
    hY  =    5.0  # y-slope
    kX  =  100.0  # x-scale
    kY  =  100.0  # y-scale

    # Coordinates
    xyS = np.meshgrid(range(nRows), range(nCols))
    xS  = xyS[1]
    yS  = xyS[0]

    # Center the coordinates and transform them in degrees
    xS = (xS-nCols/2.0)  # original version
    yS = (yS-nRows/2.0)  # original version

    # Compute complex log transform
    w_EPS = np.log(hX/kX*np.abs(xS) + hY/kY*np.abs(yS)*1j + a)

    # Uncenter the coordinates
    xTS = kX*np.sign(xS)*np.real(w_EPS)+nColsT/2.0
    yTS = kY*np.sign(yS)*np.imag(w_EPS)+nRowsT/2.0

    # Compute the radius it will cover in the cortex
    w_EP_postS = np.log((hX/kX*np.abs(xS-1) + hY/kY*np.abs(yS-1)*1j + a))
    delta_EPS  = w_EP_postS - w_EPS
    deltaS     = 0.0*np.sqrt((kX*np.real(delta_EPS))**2+(kY*np.imag(delta_EPS))**2)  # faire avec dX et dY exact locs (later)
    residualS  = deltaS - deltaS.astype(np.int)

    # Return the cortical positions and some useful variables
    return xTS.astype(np.int), yTS.astype(np.int), np.round((deltaS+0.3)).astype(np.int), residualS


# Transform the retina output image into a cortical activation map
def transformInput(img, nRows, nCols, eccentricity, camSpanAngle):

    # Generate the transformed coordinates (could be done only once per simulation, but here it is done at every time-steps)
    xTS, yTS, dS, resS = cart2cort(img.shape[0], img.shape[1], img.shape[0], img.shape[1])

    # Compute the location of the top-left pixel of the cropped cortical input
    eccTan         = [np.tan(e*np.pi/180.0) for e in eccentricity]
    camTan         = [np.tan(c*np.pi/180.0) for c in camSpanAngle]
    shift          = [e/c*s/2.0 for e,c,s in zip(eccTan, camTan, img.shape)]
    centerInputRow = int(img.shape[0]/2 + shift[0])
    centerInputCol = int(img.shape[1]/2 + shift[1])
    centerCortiRow = yTS[centerInputCol,centerInputRow]  # y is row
    centerCortiCol = xTS[centerInputCol,centerInputRow]  # x is col

    # Populate the transformed image with the original one
    imgT = np.zeros(img.shape)
    # for r in range(img.shape[0]):
    #     for c in range(img.shape[1]):
    for r in range(centerInputRow-nRows, centerInputRow+nRows):
        for c in range(centerInputCol-nCols, centerInputCol+nCols):
    # for r in range(centerInputRow-int(nRows/1.5), centerInputRow+int(nRows/1.5)):
    #     for c in range(centerInputCol-int(nCols/2), centerInputCol+int(nCols/1.5)):

            # Position of the original (c,r) pixel on the transformed map
            if (img[r,c]>0).all():
                xT, yT, d, res = xTS[c,r], yTS[c,r], dS[c,r], resS[c,r]

                # Verify that the pixel is free
                if 0 < xT < imgT.shape[1] and 0 < yT < imgT.shape[0]:
                    if imgT[yT, xT].all() == 0:
                        for xD in range(-d,d+1):
                            for yD in range(-d,d+1):

                                # Take care of large magnification
                                dist = np.sqrt(xD**2 + yD**2) 
                                if dist <= d:
                                
                                    # Populate the pixel on the transformed map
                                    if 0 < xT+xD < img.shape[1] and 0 < yT+yD < img.shape[0]:
                                        if (imgT[yT+yD, xT+xD] == 0).all():
                                            imgT[yT+yD, xT+xD] = img[r,c]

    # Fill the few pixels that are left black
    for n in [1,2,3]:
        imgT[imgT==0] = np.roll(imgT, -n, axis=0)[imgT==0]
        imgT[imgT==0] = np.roll(imgT, +n, axis=0)[imgT==0]
        imgT[imgT==0] = np.roll(imgT, -n, axis=1)[imgT==0]
        imgT[imgT==0] = np.roll(imgT, +n, axis=1)[imgT==0]

    # Return the image to the transfer function
    return imgT, centerCortiRow, centerCortiCol


# Write a custom configuration file for the brain visualizer
def writeJson(path='visualizer_tutorial.py'):

    # Parameters initialization
    currentLine = 0
    cellType    = None
    imRows      = 0
    imCols      = 0
    nOri        = 0

    # Run through the brain file to build the ID of all the populations
    popID          = []
    nNeuronTot     = 0
    descriptorKeys = ['featList', 'imRows', 'imCols']
    with open(path, 'r') as inputFile:
        for line in inputFile:

            # Look for parameters
            if 'cellType'   in line and cellType   == None:
                cellType    = re.sub('\s+', '', line.split(".")[1].split("(")[0])
            if 'nOri'       in line and nOri       == 0:
                nOri        = int(re.sub('\s+', '', line.split("=")[1].split("#")[0]))
            if 'imRows'     in line and imRows      == 0:
                imRows      = int(re.sub('\s+', '', line.split("=")[1].split("#")[0]))
                oriRows     = imRows+1
            if 'imCols'     in line and imCols      == 0:
                imCols      = int(re.sub('\s+', '', line.split("=")[1].split("#")[0]))
                oriCols     = imCols+1
                spaceOri    = int(imCols/2)

            # Look for the neuron populations
            if 'sim.Population(' in line:

                # Find the name and the type of the population
                name      =      re.sub('\s+', '', line.split('=')[0])
                typeValue = eval(re.sub('\s+', '', line.split(',')[-1])[:-1])

                # Isolate the descriptors from the nest.Create(...) lines
                descriptorValues = line.split('*')                                                          # descriptors are separated by asterisks
                descriptorValues[ 0] = re.sub('\s+', '', descriptorValues[0].split("(")[-1].split(")")[0])  # remove what's behind the first feature
                descriptorValues[-1] = re.sub('\s+', '', descriptorValues[-1].split(",")[0])                # remove what's after  the last  feature

                # Convert the string-like descriptor values into real values
                descriptorValues = [eval(valString) for valString in descriptorValues]

                # Find the number of neurons in this pop and add it to the total value
                nNeuronPop  = np.prod(descriptorValues)
                nNeuronTot += nNeuronPop

                # Take care if several features exist in the population (orientation, flow, etc.), i.e. when there is more than 4 descriptors
                descriptorValues = [list(descriptorValues[0:-2]), descriptorValues[-2], descriptorValues[-1]]

                # Create the descriptor dictionary
                descriptor = {'type': typeValue, 'line': currentLine}
                currentLine += 1
                for index, descriptorKey in enumerate(descriptorKeys):
                    descriptor[descriptorKey] = descriptorValues[index]

                # Insert the population as an entry of the popID dictionary
                popID.append((name, descriptor))

    # Open and write the file containing the neuron locations
    with open("neuronPositions.json", "w") as outputFile:
        outputFile.write('{"populations": {')
        nPopulations = len(popID)
        xDraw = -15*int(nPopulations/2)    # x-coordinate of the current set of (pop, feature) ; is incremented throughout the loop
        for popIdx in range(nPopulations):

            # Control the population is made of neurons
            popName = popID[popIdx][0]
            thisPop = popID[popIdx][1]
            if thisPop['type'] == 'IF_curr_alpha':

                # Starts writing for this population
                outputFile.write('\n\t"%s": {\n\t\t"neurons": {' % popName)
                neuronIndex = 0

                # Start to draw the neurons : x = pops, y = feature, (z, y) = (row, col)
                totalNumFeatures = np.prod(thisPop['featList'])
                for f in range(totalNumFeatures):

                    # Compute y and z coordinate origins
                    yTopLeft = (thisPop['imCols']+spaceOri)*int((f+1)/2 + 0.4)*np.power(-1, f+1) - int(thisPop['imCols']/2)
                    if totalNumFeatures%2 == 0:
                        yTopLeft = yTopLeft - int((thisPop['imCols']+spaceOri)/2)
                    zTopLeft = 0 - int(thisPop['imRows']/2)

                    # Loop throughout all the neurons of this feature inside this population
                    for zRow in range(thisPop['imRows']):
                        for yCol in range(thisPop['imCols']):

                            # Write the (x,y,z)-coordinates of this neuron
                            yDraw = yTopLeft + yCol
                            zDraw = zTopLeft + zRow
                            outputFile.write('\n\t\t\t"%s": {"pos": [%s, %s, %s]}' % (neuronIndex, xDraw, zDraw, yDraw))
                            neuronIndex += 1
                            if (f+1)*(zRow+1)*(yCol+1) < np.prod(thisPop['featList'])*thisPop['imRows']*thisPop['imCols']:
                                outputFile.write(", ")

                xDraw += 25  # space on the x-axis between each population

                # Write the end of the line
                if popIdx < nPopulations-1:
                    outputFile.write('}},')
                else:
                    outputFile.write('}}\n}}')
