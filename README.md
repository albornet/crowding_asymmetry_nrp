WP2 visual system featuring experiment
====================

This repository contains an experiment that runs several models. This includes a retina model, saliency model and a model for  visual segmentation. Together, they provide a visual system that is able to produce inward-outward anisotropy in a visual crowding paradigm. The experiment uses the HBP Neurorobotics Platform (NRP) to run the models in synchrony and to make it possible to easily activate / deactivate them for further analysis.


Installation of the experiment
-----------

**Step 1 - Install the NRP**

* For now, some models used in this experiment are not available on the online version of the NRP. To run the experiment, you have to install a local version of the NRP.
* To install the NRP locally, follow the steps from https://bitbucket.org/hbpneurorobotics/neurorobotics-platform


**Step 2 - Run the experiment installation script**

* Clone the experiment in the folder of your choice.
* Insinde this folder, run `bash install.sh`
* You will be asked if you want to install the gpu version of the saliency model. Answer 'yes' if you know that your computer can use the CUDA library and load tensorflow networks on your GPU. Because the input size is very large, running this experiment without GPU might be very heavy for your computer. I never tested it.


**Step 3 - Start the NRP**

* Start the NRP by running `cle-start && cle-nginx` in the terminal.
* Open a web browser and go to `localhost:9000/#/esv-private`


**If you want to change parameters**
 
* Experiment parameters are in $HBP/Experiments/wp2_featuring/displayStim.exd
* Change the models you want to use in $HBP/Experiments/wp2_featuring/displayStim.exd
* Parameters of the retina model are in $HBP/Experiments/wp2_featuring/TransferFunctions/RET_retina_input.py
* Parameters of the segmentation model are in $HBP/Models/brain_model/visual_segmentation3.py
* After changing parameters, you have to delete the experiment from the "My experiments" tab and reload it from the "Templates" tab before launching it again.


**Step 4 - Run the experiment**

* From the "Template" tab, clone the experiment named "WP2 featuring experiment".
* Then, from the "My experiments" tab, launch the experiment. Building the experiment might takes a long time.
* When the experiment is ready, click on the "play" button. Depending on your configuration, the simulation is very slow, because the experiments simulates many neurons and models. Also, the robot is static, so nothing will move in the simulation.
* Open several video stream windows (from the blue buttons on the left) to see the output of the different models, such as in the image below.
* When enough crowding trials have been run, the video stream topic named 'resultsPlot' displays the threshold elevation results and updates the plots after each trial. The more you run, the more precise the measure.
![Screenshot](Images/screenshot.png)


**Uninstall the experient from the NRP**

* From the cloned folder, run `bash uninstall.sh`
* Delete the clone folder.